server {
    charset utf-8;
    client_max_body_size 1024M;

    error_log  /var/log/nginx/error.log;
    access_log /var/log/nginx/access.log;

    listen 8080;
    server_name mdw.loc;
    #    server_name ${NGINX_VHOST_FRONT};

    root /app/front;
    index index.html;

    location / {
        root /app/front;
        index  index.html;
        try_files $uri  /index.html$is_args$args;
    }

    error_page 500 502 503 504 /50x.html;
    location = /50x.html {
        root /app/front/error;
    }

    location /api {
        proxy_pass http://evo_nginx:8443;
        proxy_http_version 1.1;
        proxy_buffering     off;
        proxy_max_temp_file_size 0;
        proxy_redirect     default;
        proxy_set_header   Host             $host;
        proxy_set_header   X-Real-IP        $remote_addr;
        proxy_set_header   X-Forwarded-For  $proxy_add_x_forwarded_for;
        proxy_set_header   Connection       "";
    }

    location ~ /\. {
        log_not_found off;
        deny all;
    }
}

server {
    charset utf-8;
    client_max_body_size 1024M;

    error_log  /var/log/nginx/errors_nginx_back.log;
    access_log /var/log/nginx/logger_nginx_back.log;

    listen 8443;
    server_name mdw.loc;

    root /app/back/public;
    index index.php;

   location / {
       try_files $uri /index.php$is_args$args;
   }

    location ~ \.php(/|$) {
        fastcgi_pass upstream_php;
        fastcgi_split_path_info ^(.+\.php)(/.*)$;
        include fastcgi_params;
        fastcgi_index index.php;
        fastcgi_param SCRIPT_FILENAME $realpath_root$fastcgi_script_name;
        fastcgi_param DOCUMENT_ROOT $realpath_root;
        internal;
    }
}

